package com.novikdm.view;

import com.novikdm.controller.Controller;
import com.novikdm.controller.ViewControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class ConsoleMenu {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private Scanner inputLine = new Scanner(System.in);
  private static Logger print = LogManager.getLogger(ConsoleMenu.class);

  public ConsoleMenu() {
    controller = new ViewControllerImpl();
    methodsMenu = new LinkedHashMap<>();
    menu = new LinkedHashMap<>();
    menu.put("1", "| 1 First Task |");
    menu.put("2", "| 2 Second Task |");
    menu.put("3", "| 3 Third Task |");
    menu.put("0", "| 0 Exit |");
    methodsMenu.put("1", this::firstTask);
    methodsMenu.put("2", this::secondTask);
    methodsMenu.put("3", this::thirdTask);
  }

  private void thirdTask() {
  }

  private void secondTask() {
  }

  private void firstTask() {
  }

  private void outputMenu() {
    print.trace("MENU:");
    for (String str : menu.values()) {
      print.trace(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      print.trace("Please, select menu point.");
      keyMenu = inputLine.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
